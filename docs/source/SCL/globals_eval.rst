===========
Enumerator
===========

   .. container:: contents

       
      .. rubric:: - s -
         :name: s--

      -  SCL_802_11_BAND_2_4GHZ :
         `scl_types.h <scl__types_8h.html#ad051fe45f0dabb3458843ea84a66a64da6d111a8b6f67923956f9129e9872cf0f>`__
      -  SCL_802_11_BAND_5GHZ :
         `scl_types.h <scl__types_8h.html#ad051fe45f0dabb3458843ea84a66a64dae5d47ffbe5bfd0a406b4f44d9b6a9c6c>`__
      -  SCL_AP_ROLE :
         `scl_common.h <scl__common_8h.html#a491a0d6079223bcc144618147f16b263a676bde82f54e54e5292eb421cb3e276d>`__
      -  SCL_BSS_TYPE_ADHOC :
         `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fab969be0fc08cae910fcfb820d7663ed6>`__
      -  SCL_BSS_TYPE_ANY :
         `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fa4a800e68a8f050f9faff956ce591b82a>`__
      -  SCL_BSS_TYPE_INFRASTRUCTURE :
         `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fa7b039658e58a158ce54a1219e026fc91>`__
      -  SCL_BSS_TYPE_MESH :
         `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fa643a49227a45f193bdabbef53f114488>`__
      -  SCL_BSS_TYPE_UNKNOWN :
         `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fa13a1230cb01063bb97731ce9674bc775>`__
      -  SCL_FALSE :
         `scl_common.h <scl__common_8h.html#ad0c0e3eeace86bed54540fcfb97c8812ae2b5f551535df41fa57331942b7050d0>`__
      -  SCL_INVALID_ROLE :
         `scl_common.h <scl__common_8h.html#a491a0d6079223bcc144618147f16b263a9a76612f324aa731465b8fcef1085533>`__
      -  SCL_NETWORK_RX :
         `scl_buffer_api.h <scl__buffer__api_8h.html#a170d11659d05b9fae1041771132a52a0a429fd371188f3dd267d8b7f078347479>`__
      -  SCL_NETWORK_TX :
         `scl_buffer_api.h <scl__buffer__api_8h.html#a170d11659d05b9fae1041771132a52a0abf8bfee8fd4b7ba0441cf63af353c628>`__
      -  SCL_P2P_ROLE :
         `scl_common.h <scl__common_8h.html#a491a0d6079223bcc144618147f16b263a586d733a3fb6470ef16a56dd4942da40>`__
      -  SCL_RX_DATA :
         `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8a1ef23948fa9e7e7fac502b8eb189ea47>`__
      -  SCL_RX_GET_BUFFER :
         `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8a814357835155c8c6edfb25056e992351>`__
      -  SCL_RX_GET_CONNECTION_STATUS :
         `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8a284dec35e181230494fdbb3e8db87074>`__
      -  SCL_RX_SCAN_STATUS :
         `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8a63bfd294e077ab2ac1c9f67af47f0825>`__
      -  SCL_RX_TEST_MSG :
         `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8ad41fc8f197fa39f802e1626e8a9990e2>`__
      -  SCL_RX_VERSION_COMPATIBILITY :
         `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8a11ada8cd908ee30d885a872646adaffd>`__
      -  SCL_SCAN_ABORTED :
         `scl_types.h <scl__types_8h.html#a3936863aacd5c7f6573aa494ef322a1bad4f3132ab91a0e896e7477b92dbd5075>`__
      -  SCL_SCAN_COMPLETED_SUCCESSFULLY :
         `scl_types.h <scl__types_8h.html#a3936863aacd5c7f6573aa494ef322a1bad39efc1c23ec7a0c7d67f568e6f52a85>`__
      -  SCL_SCAN_INCOMPLETE :
         `scl_types.h <scl__types_8h.html#a3936863aacd5c7f6573aa494ef322a1ba9d48c63c00486124e6bc25d8db1ec138>`__
      -  SCL_SCAN_TYPE_ACTIVE :
         `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47ea43c77a264c5a68ba7402d272d5818485>`__
      -  SCL_SCAN_TYPE_NO_BSSID_FILTER :
         `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47ea522c7c1ae2ff3acd70bc336ec14bc50e>`__
      -  SCL_SCAN_TYPE_PASSIVE :
         `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47ea325ca62baea89d873496847dc2f6e086>`__
      -  SCL_SCAN_TYPE_PNO :
         `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47eadbab2440a09df91733882418c643c607>`__
      -  SCL_SCAN_TYPE_PROHIBITED_CHANNELS :
         `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47ea9e84ff288b7e8c32359d5d6c1898804d>`__
      -  SCL_SECURITY_FORCE_32_BIT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a35af276654937141437e8c912966ac73>`__
      -  SCL_SECURITY_IBSS_OPEN :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ad886638286e66e06c8e3fa714146c9a0>`__
      -  SCL_SECURITY_OPEN :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a86f904c2b846eef773b9200212046dcd>`__
      -  SCL_SECURITY_UNKNOWN :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a8bda7dcf292a27fe457096fbabeb59eb>`__
      -  SCL_SECURITY_WEP_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ad48007b9e1fe1972d5c4070642d7f26b>`__
      -  SCL_SECURITY_WEP_SHARED :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ab7c67fff083e10fe48d11c29558b1d39>`__
      -  SCL_SECURITY_WPA2_AES_ENT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67af5c5e8ba6b8c3de871d3c989b2691a4e>`__
      -  SCL_SECURITY_WPA2_AES_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67aa88617f6384eea8a0f7a6abe0f84ec81>`__
      -  SCL_SECURITY_WPA2_FBT_ENT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a0e83dca81b6fe104e6f9cf45a07bac80>`__
      -  SCL_SECURITY_WPA2_FBT_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a5663ce3870a19a2d01c816e54417f2ba>`__
      -  SCL_SECURITY_WPA2_MIXED_ENT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67aa23b01657d269dadc56692a9655ca69b>`__
      -  SCL_SECURITY_WPA2_MIXED_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ae28655d240b846e6cfd00676704d322d>`__
      -  SCL_SECURITY_WPA2_TKIP_ENT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67adccfe4e43b721376adeb3a5bac94a714>`__
      -  SCL_SECURITY_WPA2_TKIP_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ae1efdd5c152b318166d6f6560393261e>`__
      -  SCL_SECURITY_WPA3_SAE :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a23e95b5998fa405fecd423e416cff179>`__
      -  SCL_SECURITY_WPA3_WPA2_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a5d9d2fe7c0130877be4e2f8d1d7f2311>`__
      -  SCL_SECURITY_WPA_AES_ENT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67acee17758cf43103a3a3b8736f7ffa032>`__
      -  SCL_SECURITY_WPA_AES_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a29a27e98cf3c95da855b5577825d270e>`__
      -  SCL_SECURITY_WPA_MIXED_ENT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67aa8e4e60ef3977f23abae498e391dcb81>`__
      -  SCL_SECURITY_WPA_MIXED_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a328af64a5bfe6959a75d150bb500f4fe>`__
      -  SCL_SECURITY_WPA_TKIP_ENT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a644c609edc87ebba035f2de49ac62f1a>`__
      -  SCL_SECURITY_WPA_TKIP_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ae98e5d51446406bd53805807f40c70b8>`__
      -  SCL_SECURITY_WPS_OPEN :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a42bca33cf42db8341589161fc90a08ac>`__
      -  SCL_SECURITY_WPS_SECURE :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ac25ebb997080ba93473a3171bb168393>`__
      -  SCL_STA_ROLE :
         `scl_common.h <scl__common_8h.html#a491a0d6079223bcc144618147f16b263a8f8f3eacf8c09454172a590657d78afc>`__
      -  SCL_TRUE :
         `scl_common.h <scl__common_8h.html#ad0c0e3eeace86bed54540fcfb97c8812a25c4ff8d5a12ed99b5d6f0dc175ea47d>`__
      -  SCL_TX_CONFIG_PARAMETERS :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46affbde7bce68d8585e5dc6f811e534aec>`__
      -  SCL_TX_CONNECT :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a234e28ed2e4ef5b47b7dec4315b87dc5>`__
      -  SCL_TX_CONNECTION_STATUS :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a1833ff3bb744ded966d7e303f661d584>`__
      -  SCL_TX_DISCONNECT :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a404a155243899240e4066042d05f40bb>`__
      -  SCL_TX_GET_MAC :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46aa83a436dbdb4c3f2a0f30cb007db706a>`__
      -  SCL_TX_REGISTER_MULTICAST_ADDRESS :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46af78db32b364393f00bdfa8532851ffd0>`__
      -  SCL_TX_SCAN :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a819078a6ac44ea3c25516668db264967>`__
      -  SCL_TX_SCL_VERSION_NUMBER :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46ab469ff0c31036aedc93e0e19f3acc749>`__
      -  SCL_TX_SEND_OUT :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46affc21552e23ab5fb0f3467063a01cc02>`__
      -  SCL_TX_TEST_MSG :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a58efee8e7e8769d8685fc1b6567ed911>`__
      -  SCL_TX_TRANSCEIVE_READY :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a8b0a03e5769e80b5ac4bfece90968f7c>`__
      -  SCL_TX_WIFI_GET_BSSID :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a9cb54f84da32c6a983adc1f96890ce4b>`__
      -  SCL_TX_WIFI_GET_RSSI :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46ac52dd1ad608863e2561bd91c2cc76dfc>`__
      -  SCL_TX_WIFI_INIT :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a8578fe2e7e153cbb90238ddab4578472>`__
      -  SCL_TX_WIFI_NW_PARAM :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46aeba990e60a370115fddfab01fb80efbc>`__
      -  SCL_TX_WIFI_ON :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46ab59880910176cc776f5b44520068bb9c>`__
      -  SCL_TX_WIFI_SET_UP :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a88fe5a393e75e5c10f9f18eaeba1cd5a>`__


