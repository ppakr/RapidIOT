===============================
SubSystems Communication Layer
===============================



 
       

      .. rubric:: SCL Overview
         :name: scl-overview

      The SCL is developed to act as an interface layer between the
      network stack and Network Processor. It provides APIs to interact
      with the Network Processor. General network related operations
      like connecting to a network, transmitting packets and receiving
      packets can be performed by using NetworkInterface, WiFiInterface
      or SclSTAInterface objects. The SCL uses Inter-Processor
      Communication (IPC) for communicating with the Network Processor.

      .. rubric:: SCL Features
         :name: scl-features

      -  Relays data to and from the Network Processor
      -  Allocates a buffer for communicating with the Network Processor
      -  Supports Wi-Fi station (STA) mode of operation

      .. rubric:: SCL Folder Structure
         :name: scl-folder-structure

      -  features\net-socket\emac-drivers\TARGET_Cypress\COMPONENT_SCL\interface\SclSTAInterface.h
         - Contains APIs that can be accessed using NetworkInterface or
         WiFiInterface objects.
      -  features\net-socket\emac-drivers\TARGET_Cypress\COMPONENT_SCL\interface\scl_emac.h
         - Contains APIs that can be accessed using EMACInterface
         object.
      -  targets\TARGET_Cypress\TARGET_PSOC6\COMPONENT_SCL\inc\scl_common.h
         - Contains common data types and error codes used in the SCL.
      -  targets\TARGET_Cypress\TARGET_PSOC6\COMPONENT_SCL\inc\scl_ipc.h
         - Contains APIs used by the SCL to interact with the Network
         Processor.
      -  targets\TARGET_Cypress\TARGET_PSOC6\COMPONENT_SCL\inc\scl_types.h
         - Contains definitions for Wi-Fi security modes.
      -  targets\TARGET_Cypress\TARGET_PSOC6\COMPONENT_SCL\inc\scl_wifi_api.h
         - Contains APIs to directly interact with the Network Processor
         without using any mbed-os class objects.
      -  targets\TARGET_Cypress\TARGET_PSOC6\COMPONENT_SCL\src\include\scl_buffer_api.h
         - Contains APIs to allocate and de-allocate memory.

      .. rubric:: SCL Architecture
         :name: scl-architecture

      

      .. rubric:: Getting started with SCL APIs
         :name: getting-started-with-scl-apis

      Below is a sample application for using SCL.

      .. rubric:: Sample Application:
         :name: sample-application

      ::

          
         #include "scl_ipc.h"

         int main(void)
         {
             scl_init(); //must be included while using SCL 
             
             /*Insert your application here*/
             
         }
		 
.. toctree::
   :maxdepth: 8
   :hidden:
   
   modules.rst
   data_structures.rst
   files_list.rst


