==
s
==

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented functions, variables, defines,
         enums, and typedefs with links to the documentation:

      .. rubric:: - s -
         :name: s--

      -  SCL_802_11_BAND_2_4GHZ :
         `scl_types.h <scl__types_8h.html#ad051fe45f0dabb3458843ea84a66a64da6d111a8b6f67923956f9129e9872cf0f>`__
      -  SCL_802_11_BAND_5GHZ :
         `scl_types.h <scl__types_8h.html#ad051fe45f0dabb3458843ea84a66a64dae5d47ffbe5bfd0a406b4f44d9b6a9c6c>`__
      -  scl_802_11_band_t :
         `scl_types.h <scl__types_8h.html#ad051fe45f0dabb3458843ea84a66a64d>`__
      -  SCL_ACCESS_POINT_NOT_FOUND :
         `scl_common.h <scl__common_8h.html#a4c4edaa8649ebd374238c479c1c6d787>`__
      -  SCL_ADDRESS_ALREADY_REGISTERED :
         `scl_common.h <scl__common_8h.html#a694c381b90996fe878150bb617904292>`__
      -  SCL_AP_ALREADY_UP :
         `scl_common.h <scl__common_8h.html#aa3b57f3b3f84f6414fbc49955b52323b>`__
      -  SCL_AP_ROLE :
         `scl_common.h <scl__common_8h.html#a491a0d6079223bcc144618147f16b263a676bde82f54e54e5292eb421cb3e276d>`__
      -  SCL_BADARG :
         `scl_common.h <scl__common_8h.html#ae6acee12ba2f2c10646820a5318f6bd6>`__
      -  scl_bool_t :
         `scl_common.h <scl__common_8h.html#ad0c0e3eeace86bed54540fcfb97c8812>`__
      -  SCL_BSS_TYPE_ADHOC :
         `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fab969be0fc08cae910fcfb820d7663ed6>`__
      -  SCL_BSS_TYPE_ANY :
         `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fa4a800e68a8f050f9faff956ce591b82a>`__
      -  SCL_BSS_TYPE_INFRASTRUCTURE :
         `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fa7b039658e58a158ce54a1219e026fc91>`__
      -  SCL_BSS_TYPE_MESH :
         `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fa643a49227a45f193bdabbef53f114488>`__
      -  scl_bss_type_t :
         `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92f>`__
      -  SCL_BSS_TYPE_UNKNOWN :
         `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fa13a1230cb01063bb97731ce9674bc775>`__
      -  SCL_BUFFER_ALLOC_FAIL :
         `scl_common.h <scl__common_8h.html#afe96ddd63a5e911ab9734e431b010db7>`__
      -  scl_buffer_dir_t :
         `scl_buffer_api.h <scl__buffer__api_8h.html#a170d11659d05b9fae1041771132a52a0>`__
      -  scl_buffer_get_current_piece_data_pointer() :
         `scl_buffer_api.h <scl__buffer__api_8h.html#aa19779310d2e60bf7037932f421ef514>`__
      -  scl_buffer_get_current_piece_size() :
         `scl_buffer_api.h <scl__buffer__api_8h.html#ae9f16743fd6279a56c22f97335f379d9>`__
      -  SCL_BUFFER_POINTER_MOVE_ERROR :
         `scl_common.h <scl__common_8h.html#aacd2dd6dadbf4220d362decd3ec4cc80>`__
      -  scl_buffer_release() :
         `scl_buffer_api.h <scl__buffer__api_8h.html#a1275123a0b9434a2c3a2d64566269087>`__
      -  SCL_BUFFER_SIZE_SET_ERROR :
         `scl_common.h <scl__common_8h.html#a5882aa767bfe88aaad24512afa97a861>`__
      -  scl_buffer_t :
         `scl_common.h <scl__common_8h.html#ade53c1ba949b59704f58c0c2d11d5421>`__
      -  SCL_BUFFER_UNAVAILABLE_PERMANENT :
         `scl_common.h <scl__common_8h.html#abc10ed772792a39e89980135b9b831f9>`__
      -  SCL_BUFFER_UNAVAILABLE_TEMPORARY :
         `scl_common.h <scl__common_8h.html#ab9d5cb581d942c047b392dc7c965fd88>`__
      -  SCL_BUS_READ_REGISTER_ERROR :
         `scl_common.h <scl__common_8h.html#a82489c6172fc349133b0faf86776f2cb>`__
      -  SCL_BUS_WRITE_REGISTER_ERROR :
         `scl_common.h <scl__common_8h.html#a788952b9f0e276d8df9ee3de610b05b2>`__
      -  SCL_CLM_BLOB_DLOAD_ERROR :
         `scl_common.h <scl__common_8h.html#a7fc0f4f4394fc5467e87d3ba3305d484>`__
      -  SCL_CONNECTION_LOST :
         `scl_common.h <scl__common_8h.html#a147164e3c59553e9dd10006b2177cb7e>`__
      -  SCL_CORE_CLOCK_NOT_ENABLED :
         `scl_common.h <scl__common_8h.html#ae5012dfc795a129a3f64714525aa1e02>`__
      -  SCL_CORE_IN_RESET :
         `scl_common.h <scl__common_8h.html#a46d67d50f44ebbe2d2b516b58f8b20ae>`__
      -  SCL_DELAY_TOO_LONG :
         `scl_common.h <scl__common_8h.html#adf604df3c574c0ba6a87300b72a3ba30>`__
      -  SCL_DELAY_TOO_SHORT :
         `scl_common.h <scl__common_8h.html#ae13e62ab3ec077effdf3bc66ae626e74>`__
      -  SCL_DOES_NOT_EXIST :
         `scl_common.h <scl__common_8h.html#a27f8ccfef1cfdfd307d64a5325498938>`__
      -  SCL_EAPOL_KEY_FAILURE :
         `scl_common.h <scl__common_8h.html#a168b46abdc05efad43c0a11338a7bab2>`__
      -  SCL_EAPOL_KEY_PACKET_G1_TIMEOUT :
         `scl_common.h <scl__common_8h.html#a34c3c4d9dbe5515f50529776d61fa2f0>`__
      -  SCL_EAPOL_KEY_PACKET_M1_TIMEOUT :
         `scl_common.h <scl__common_8h.html#a4e9f540f4c64973328c2b56b1bdf0044>`__
      -  SCL_EAPOL_KEY_PACKET_M3_TIMEOUT :
         `scl_common.h <scl__common_8h.html#a5484a20a77467db4f145b2eb8578c910>`__
      -  scl_emac_wifi_link_state_changed() :
         `scl_emac.h <scl__emac_8h.html#a84ab01d9a18dca2e31b27c125bcb8a7f>`__
         ,
         `scl_wifi_api.h <group__wifi.html#ga84ab01d9a18dca2e31b27c125bcb8a7f>`__
      -  scl_end() :
         `scl_ipc.h <group__communication.html#gad01b73140538bb57e28ad911dd62412f>`__
      -  SCL_ERROR :
         `scl_common.h <scl__common_8h.html#afcef196f4c8d543377091c20a821882a>`__
      -  SCL_FALSE :
         `scl_common.h <scl__common_8h.html#ad0c0e3eeace86bed54540fcfb97c8812ae2b5f551535df41fa57331942b7050d0>`__
      -  SCL_FILTER_NOT_FOUND :
         `scl_common.h <scl__common_8h.html#a94ee0fc58fef7785b8f62e3c9178a448>`__
      -  SCL_FLOW_CONTROLLED :
         `scl_common.h <scl__common_8h.html#afc83c8f84b293410b50fc799ef3e50fc>`__
      -  scl_get_nw_parameters() :
         `scl_ipc.h <group__communication.html#ga690f0ae086500cbc92390cb2c0d0a429>`__
      -  SCL_HAL_ERROR :
         `scl_common.h <scl__common_8h.html#ab0a878abdbe284a38fdc85536016a07d>`__
      -  SCL_HANDLER_ALREADY_REGISTERED :
         `scl_common.h <scl__common_8h.html#a717fe3a815914d29e13080e72d44502c>`__
      -  scl_host_buffer_get() :
         `scl_buffer_api.h <scl__buffer__api_8h.html#ad46d8cab5cb70bcdbb7d1168c5a71976>`__
      -  SCL_HWTAG_MISMATCH :
         `scl_common.h <scl__common_8h.html#a8545cf3396f9472f70baec87b26ca975>`__
      -  scl_init() :
         `scl_ipc.h <group__communication.html#gaa0e2decd5df1e776b053ddf33e7cd16d>`__
      -  SCL_INTERFACE_NOT_UP :
         `scl_common.h <scl__common_8h.html#a98026fc9756213cb3717749fb749f7cd>`__
      -  scl_interface_role_t :
         `scl_common.h <scl__common_8h.html#a491a0d6079223bcc144618147f16b263>`__
      -  SCL_INVALID_DUTY_CYCLE :
         `scl_common.h <scl__common_8h.html#ad521e106005c683ab61d9615b0969dbb>`__
      -  SCL_INVALID_INTERFACE :
         `scl_common.h <scl__common_8h.html#ad6140ba3485a938ca3f65870468d9465>`__
      -  SCL_INVALID_JOIN_STATUS :
         `scl_common.h <scl__common_8h.html#a5897b37122798b4a186e9c7a4c1869d9>`__
      -  SCL_INVALID_KEY :
         `scl_common.h <scl__common_8h.html#a3b229cb9def5d820651c47911b64cfb8>`__
      -  SCL_INVALID_ROLE :
         `scl_common.h <scl__common_8h.html#a491a0d6079223bcc144618147f16b263a9a76612f324aa731465b8fcef1085533>`__
      -  SCL_IOCTL_FAIL :
         `scl_common.h <scl__common_8h.html#acb9d8da07a1ebb8b750405d20d44c0ca>`__
      -  scl_ipc_rx_t :
         `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8>`__
      -  scl_ipc_tx_t :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46>`__
      -  SCL_JOIN_IN_PROGRESS :
         `scl_common.h <scl__common_8h.html#a3b49eceb733774d128be1a246f3c520e>`__
      -  SCL_LOG :
         `scl_common.h <scl__common_8h.html#afe0916a4a7aa61aec705c0eea7070ec3>`__
      -  SCL_LOG_ENABLE :
         `scl_common.h <scl__common_8h.html#a39c911a3ca34cd81bdd64bd96b948b6a>`__
      -  SCL_MAJOR_VERSION :
         `scl_version.h <scl__version_8h.html#a7fd7185102136f36fc79987d0e28cdd2>`__
      -  SCL_MALLOC_FAILURE :
         `scl_common.h <scl__common_8h.html#ae5ddfcf29e6075b791683f2edde7a33b>`__
      -  SCL_MINOR_VERSION :
         `scl_version.h <scl__version_8h.html#a134873d963fcda738ed2213e8fbe4e82>`__
      -  SCL_NETWORK_NOT_FOUND :
         `scl_common.h <scl__common_8h.html#a3e135af2999d2a2912c87330b1c88681>`__
      -  scl_network_process_ethernet_data() :
         `scl_wifi_api.h <group__wifi.html#ga6ffc9c85fcef342276617b83afa610a7>`__
      -  SCL_NETWORK_RX :
         `scl_buffer_api.h <scl__buffer__api_8h.html#a170d11659d05b9fae1041771132a52a0a429fd371188f3dd267d8b7f078347479>`__
      -  scl_network_send_ethernet_data() :
         `scl_wifi_api.h <group__wifi.html#ga056399731dbe8a81c76186a0ee9626d6>`__
      -  SCL_NETWORK_TX :
         `scl_buffer_api.h <scl__buffer__api_8h.html#a170d11659d05b9fae1041771132a52a0abf8bfee8fd4b7ba0441cf63af353c628>`__
      -  SCL_NO_CREDITS :
         `scl_common.h <scl__common_8h.html#a41d7fa4a6b22c656ec08f2408c5b493a>`__
      -  SCL_NO_PACKET_TO_RECEIVE :
         `scl_common.h <scl__common_8h.html#a2339dba8b136ea2d0eb526892ef6822e>`__
      -  SCL_NO_PACKET_TO_SEND :
         `scl_common.h <scl__common_8h.html#a63b966b4f32b5a30b33ad391214fbe52>`__
      -  SCL_NOT_AUTHENTICATED :
         `scl_common.h <scl__common_8h.html#a6ad3b22c1e2869653dd5ecd612fc4673>`__
      -  SCL_NOT_KEYED :
         `scl_common.h <scl__common_8h.html#a13c2a01881f8198c433e37c000befc39>`__
      -  SCL_NULL_PTR_ARG :
         `scl_common.h <scl__common_8h.html#a2d325860a6c6f788f5c64894dd890839>`__
      -  SCL_OUT_OF_EVENT_HANDLER_SPACE :
         `scl_common.h <scl__common_8h.html#ae93602b58610afc8529b2178e3e2c5af>`__
      -  SCL_P2P_ROLE :
         `scl_common.h <scl__common_8h.html#a491a0d6079223bcc144618147f16b263a586d733a3fb6470ef16a56dd4942da40>`__
      -  SCL_PARTIAL_RESULTS :
         `scl_common.h <scl__common_8h.html#a92ca5f463e1c2a83310e5df81491e0d4>`__
      -  SCL_PATCH_VERSION :
         `scl_version.h <scl__version_8h.html#a44e529b5666aed069f63bba707cc9945>`__
      -  SCL_PAYLOAD_MTU :
         `scl_common.h <scl__common_8h.html#aa4651e54d79668879d137dfda561c7af>`__
      -  SCL_PENDING :
         `scl_common.h <scl__common_8h.html#ae57e1328e4bd1d8ec34471b6088f7881>`__
      -  SCL_PMK_WRONG_LENGTH :
         `scl_common.h <scl__common_8h.html#aad172962be0f5647656e31f8203b69b4>`__
      -  SCL_QUEUE_ERROR :
         `scl_common.h <scl__common_8h.html#a700f846b3efab9ccbda3b2663c407603>`__
      -  SCL_RESULT_CREATE :
         `scl_common.h <scl__common_8h.html#ac2ae61090a6b87a10cba31d3a7ab7dbc>`__
      -  scl_result_t :
         `scl_common.h <scl__common_8h.html#aa58a3303ad0c17b7a2d32b4b3f390ec4>`__
      -  SCL_RESULT_TYPE :
         `scl_common.h <scl__common_8h.html#aa4b42e44356c70b6978ac8bfbba52d5e>`__
      -  SCL_RTOS_ERROR :
         `scl_common.h <scl__common_8h.html#a23d7912477b9d69ebcccd788dc0d72cf>`__
      -  SCL_RTOS_STATIC_MEM_LIMIT :
         `scl_common.h <scl__common_8h.html#ab2842cd2bb9d0d2f9b881a072f932298>`__
      -  SCL_RX_BUFFER_ALLOC_FAIL :
         `scl_common.h <scl__common_8h.html#a3afdd559df35bb55e420581884284587>`__
      -  SCL_RX_DATA :
         `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8a1ef23948fa9e7e7fac502b8eb189ea47>`__
      -  SCL_RX_GET_BUFFER :
         `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8a814357835155c8c6edfb25056e992351>`__
      -  SCL_RX_GET_CONNECTION_STATUS :
         `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8a284dec35e181230494fdbb3e8db87074>`__
      -  SCL_RX_SCAN_STATUS :
         `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8a63bfd294e077ab2ac1c9f67af47f0825>`__
      -  SCL_RX_TEST_MSG :
         `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8ad41fc8f197fa39f802e1626e8a9990e2>`__
      -  SCL_RX_VERSION_COMPATIBILITY :
         `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8a11ada8cd908ee30d885a872646adaffd>`__
      -  SCL_SCAN_ABORTED :
         `scl_types.h <scl__types_8h.html#a3936863aacd5c7f6573aa494ef322a1bad4f3132ab91a0e896e7477b92dbd5075>`__
      -  SCL_SCAN_COMPLETED_SUCCESSFULLY :
         `scl_types.h <scl__types_8h.html#a3936863aacd5c7f6573aa494ef322a1bad39efc1c23ec7a0c7d67f568e6f52a85>`__
      -  SCL_SCAN_INCOMPLETE :
         `scl_types.h <scl__types_8h.html#a3936863aacd5c7f6573aa494ef322a1ba9d48c63c00486124e6bc25d8db1ec138>`__
      -  scl_scan_result_callback_t :
         `scl_wifi_api.h <group__wifi.html#gaf28de372f19220528a8020d489283291>`__
      -  scl_scan_result_t :
         `scl_types.h <scl__types_8h.html#a5ddf8e1e228e60b1759b668540e30a5f>`__
      -  scl_scan_status_t :
         `scl_types.h <scl__types_8h.html#a3936863aacd5c7f6573aa494ef322a1b>`__
      -  SCL_SCAN_TYPE_ACTIVE :
         `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47ea43c77a264c5a68ba7402d272d5818485>`__
      -  SCL_SCAN_TYPE_NO_BSSID_FILTER :
         `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47ea522c7c1ae2ff3acd70bc336ec14bc50e>`__
      -  SCL_SCAN_TYPE_PASSIVE :
         `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47ea325ca62baea89d873496847dc2f6e086>`__
      -  SCL_SCAN_TYPE_PNO :
         `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47eadbab2440a09df91733882418c643c607>`__
      -  SCL_SCAN_TYPE_PROHIBITED_CHANNELS :
         `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47ea9e84ff288b7e8c32359d5d6c1898804d>`__
      -  scl_scan_type_t :
         `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47e>`__
      -  SCL_SDIO_BUS_UP_FAIL :
         `scl_common.h <scl__common_8h.html#a592ef2a003492d6e82e3f7bca65ca296>`__
      -  SCL_SDIO_RETRIES_EXCEEDED :
         `scl_common.h <scl__common_8h.html#a961738b00ab6cc3fd0a895684b7d7bc3>`__
      -  SCL_SDIO_RX_FAIL :
         `scl_common.h <scl__common_8h.html#ac8be2d2070d1112b1ae8c6ee457dacb0>`__
      -  SCL_SECURITY_FORCE_32_BIT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a35af276654937141437e8c912966ac73>`__
      -  SCL_SECURITY_IBSS_OPEN :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ad886638286e66e06c8e3fa714146c9a0>`__
      -  SCL_SECURITY_OPEN :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a86f904c2b846eef773b9200212046dcd>`__
      -  scl_security_t :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67>`__
      -  SCL_SECURITY_UNKNOWN :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a8bda7dcf292a27fe457096fbabeb59eb>`__
      -  SCL_SECURITY_WEP_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ad48007b9e1fe1972d5c4070642d7f26b>`__
      -  SCL_SECURITY_WEP_SHARED :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ab7c67fff083e10fe48d11c29558b1d39>`__
      -  SCL_SECURITY_WPA2_AES_ENT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67af5c5e8ba6b8c3de871d3c989b2691a4e>`__
      -  SCL_SECURITY_WPA2_AES_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67aa88617f6384eea8a0f7a6abe0f84ec81>`__
      -  SCL_SECURITY_WPA2_FBT_ENT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a0e83dca81b6fe104e6f9cf45a07bac80>`__
      -  SCL_SECURITY_WPA2_FBT_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a5663ce3870a19a2d01c816e54417f2ba>`__
      -  SCL_SECURITY_WPA2_MIXED_ENT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67aa23b01657d269dadc56692a9655ca69b>`__
      -  SCL_SECURITY_WPA2_MIXED_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ae28655d240b846e6cfd00676704d322d>`__
      -  SCL_SECURITY_WPA2_TKIP_ENT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67adccfe4e43b721376adeb3a5bac94a714>`__
      -  SCL_SECURITY_WPA2_TKIP_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ae1efdd5c152b318166d6f6560393261e>`__
      -  SCL_SECURITY_WPA3_SAE :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a23e95b5998fa405fecd423e416cff179>`__
      -  SCL_SECURITY_WPA3_WPA2_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a5d9d2fe7c0130877be4e2f8d1d7f2311>`__
      -  SCL_SECURITY_WPA_AES_ENT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67acee17758cf43103a3a3b8736f7ffa032>`__
      -  SCL_SECURITY_WPA_AES_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a29a27e98cf3c95da855b5577825d270e>`__
      -  SCL_SECURITY_WPA_MIXED_ENT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67aa8e4e60ef3977f23abae498e391dcb81>`__
      -  SCL_SECURITY_WPA_MIXED_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a328af64a5bfe6959a75d150bb500f4fe>`__
      -  SCL_SECURITY_WPA_TKIP_ENT :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a644c609edc87ebba035f2de49ac62f1a>`__
      -  SCL_SECURITY_WPA_TKIP_PSK :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ae98e5d51446406bd53805807f40c70b8>`__
      -  SCL_SECURITY_WPS_OPEN :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a42bca33cf42db8341589161fc90a08ac>`__
      -  SCL_SECURITY_WPS_SECURE :
         `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ac25ebb997080ba93473a3171bb168393>`__
      -  SCL_SEMAPHORE_ERROR :
         `scl_common.h <scl__common_8h.html#a77575fc99109a3953ef1e6c4f7c9be48>`__
      -  scl_send_data() :
         `scl_ipc.h <group__communication.html#ga4cf63cea8e5e196353b80d769b7182ff>`__
      -  SCL_SET_BLOCK_ACK_WINDOW_FAIL :
         `scl_common.h <scl__common_8h.html#af4097c312e5ee146c0519ae2162b7b55>`__
      -  SCL_SLEEP_ERROR :
         `scl_common.h <scl__common_8h.html#a6c9a25e621bcf2c49b2eea0273382534>`__
      -  SCL_SPI_ID_READ_FAIL :
         `scl_common.h <scl__common_8h.html#a9b9f49b2803ff123f9a4b72a50295bf1>`__
      -  SCL_SPI_SIZE_MISMATCH :
         `scl_common.h <scl__common_8h.html#a9125da1d1d934636999907d5d709ce0e>`__
      -  SCL_STA_ROLE :
         `scl_common.h <scl__common_8h.html#a491a0d6079223bcc144618147f16b263a8f8f3eacf8c09454172a590657d78afc>`__
      -  SCL_SUCCESS :
         `scl_common.h <scl__common_8h.html#afea4b7a41665fefdc814c971d1b0e893>`__
      -  scl_sync_scan_result_t :
         `scl_types.h <scl__types_8h.html#aa40820ffed06905bfcf538935c43d1f7>`__
      -  SCL_THREAD_CREATE_FAILED :
         `scl_common.h <scl__common_8h.html#ae68f734c5fbed881e2ec0c001867232a>`__
      -  SCL_THREAD_DELETE_FAIL :
         `scl_common.h <scl__common_8h.html#a4304ef6c5c45329d188cfe066bf02d17>`__
      -  SCL_THREAD_FINISH_FAIL :
         `scl_common.h <scl__common_8h.html#a266b3500ad62ffa18e1069f5f183bb8f>`__
      -  SCL_THREAD_STACK_NULL :
         `scl_common.h <scl__common_8h.html#a81ae876334eab64ba3f3c52b14eae291>`__
      -  SCL_TIMEOUT :
         `scl_common.h <scl__common_8h.html#a4708918902cf76addf76a04f59ba47e8>`__
      -  SCL_TRUE :
         `scl_common.h <scl__common_8h.html#ad0c0e3eeace86bed54540fcfb97c8812a25c4ff8d5a12ed99b5d6f0dc175ea47d>`__
      -  scl_tx_buf_t :
         `scl_wifi_api.h <scl__wifi__api_8h.html#a9992f26a207c7256ba19dabbb1d86216>`__
      -  SCL_TX_CONFIG_PARAMETERS :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46affbde7bce68d8585e5dc6f811e534aec>`__
      -  SCL_TX_CONNECT :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a234e28ed2e4ef5b47b7dec4315b87dc5>`__
      -  SCL_TX_CONNECTION_STATUS :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a1833ff3bb744ded966d7e303f661d584>`__
      -  SCL_TX_DISCONNECT :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a404a155243899240e4066042d05f40bb>`__
      -  SCL_TX_GET_MAC :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46aa83a436dbdb4c3f2a0f30cb007db706a>`__
      -  SCL_TX_REGISTER_MULTICAST_ADDRESS :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46af78db32b364393f00bdfa8532851ffd0>`__
      -  SCL_TX_SCAN :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a819078a6ac44ea3c25516668db264967>`__
      -  SCL_TX_SCL_VERSION_NUMBER :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46ab469ff0c31036aedc93e0e19f3acc749>`__
      -  SCL_TX_SEND_OUT :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46affc21552e23ab5fb0f3467063a01cc02>`__
      -  SCL_TX_TEST_MSG :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a58efee8e7e8769d8685fc1b6567ed911>`__
      -  SCL_TX_TRANSCEIVE_READY :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a8b0a03e5769e80b5ac4bfece90968f7c>`__
      -  SCL_TX_WIFI_GET_BSSID :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a9cb54f84da32c6a983adc1f96890ce4b>`__
      -  SCL_TX_WIFI_GET_RSSI :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46ac52dd1ad608863e2561bd91c2cc76dfc>`__
      -  SCL_TX_WIFI_INIT :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a8578fe2e7e153cbb90238ddab4578472>`__
      -  SCL_TX_WIFI_NW_PARAM :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46aeba990e60a370115fddfab01fb80efbc>`__
      -  SCL_TX_WIFI_ON :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46ab59880910176cc776f5b44520068bb9c>`__
      -  SCL_TX_WIFI_SET_UP :
         `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a88fe5a393e75e5c10f9f18eaeba1cd5a>`__
      -  SCL_UNFINISHED :
         `scl_common.h <scl__common_8h.html#a622ce501adf697709e64a1d536af64b0>`__
      -  SCL_UNKNOWN_INTERFACE :
         `scl_common.h <scl__common_8h.html#a2eaf58b9ca04ab501de5de179343d064>`__
      -  SCL_UNKNOWN_SECURITY_TYPE :
         `scl_common.h <scl__common_8h.html#ad10521dcba2930b53b3ba874cca55651>`__
      -  SCL_UNSUPPORTED :
         `scl_common.h <scl__common_8h.html#a322e94949e22bcd0a7543b37d6f7db9e>`__
      -  SCL_WAIT_ABORTED :
         `scl_common.h <scl__common_8h.html#a90f5a1884be7a65b04cf1c8af0721ad0>`__
      -  SCL_WEP_KEYLEN_BAD :
         `scl_common.h <scl__common_8h.html#a228b544da9140f31627089f0ab5b7320>`__
      -  SCL_WEP_NOT_ALLOWED :
         `scl_common.h <scl__common_8h.html#a67920030a5d135b0caa6dca8cd71e495>`__
      -  scl_wifi_get_bssid() :
         `scl_wifi_api.h <group__wifi.html#gaa10fb0d690e07935fcb5c2ec35c2029b>`__
      -  scl_wifi_get_mac_address() :
         `scl_wifi_api.h <group__wifi.html#gaeab95b84a48631c4ad535d664c990a43>`__
      -  scl_wifi_get_rssi() :
         `scl_wifi_api.h <group__wifi.html#gaadf11fa39efd4aeb7d4b9efc396f6c24>`__
      -  scl_wifi_is_ready_to_transceive() :
         `scl_wifi_api.h <group__wifi.html#ga7ee9f25797137c03c2f1ad08481b11cf>`__
      -  scl_wifi_on() :
         `scl_wifi_api.h <group__wifi.html#gabb29fb3b65d9a0bffe66afea8d3244da>`__
      -  scl_wifi_register_multicast_address() :
         `scl_wifi_api.h <group__wifi.html#ga22cc8cb41c67b3a3cc1fbcdabb3b6b3f>`__
      -  scl_wifi_scan() :
         `scl_wifi_api.h <group__wifi.html#gaed2249e7300ad43ef519857cd04e4714>`__
      -  scl_wifi_set_up() :
         `scl_wifi_api.h <group__wifi.html#ga5f30d059fce225b6a1bb1d6b63747fa5>`__
      -  SCL_WPA_KEYLEN_BAD :
         `scl_common.h <scl__common_8h.html#aa2fb9dcf5bb1ded56653f15651c09e3b>`__
      -  SHARED_ENABLED :
         `scl_types.h <scl__types_8h.html#a4d4a4586c264fe8e4acb0bf7169b7b0f>`__
      -  SSID_NAME_SIZE :
         `scl_types.h <scl__types_8h.html#a9ee2fe056ad3787e30bb8da7248592c7>`__

