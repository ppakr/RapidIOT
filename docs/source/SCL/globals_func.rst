==========
Functions
==========

   .. container:: contents

       

      -  scl_buffer_get_current_piece_data_pointer() :
         `scl_buffer_api.h <scl__buffer__api_8h.html#aa19779310d2e60bf7037932f421ef514>`__
      -  scl_buffer_get_current_piece_size() :
         `scl_buffer_api.h <scl__buffer__api_8h.html#ae9f16743fd6279a56c22f97335f379d9>`__
      -  scl_buffer_release() :
         `scl_buffer_api.h <scl__buffer__api_8h.html#a1275123a0b9434a2c3a2d64566269087>`__
      -  scl_emac_wifi_link_state_changed() :
         `scl_emac.h <scl__emac_8h.html#a84ab01d9a18dca2e31b27c125bcb8a7f>`__
         ,
         `scl_wifi_api.h <group__wifi.html#ga84ab01d9a18dca2e31b27c125bcb8a7f>`__
      -  scl_end() :
         `scl_ipc.h <group__communication.html#gad01b73140538bb57e28ad911dd62412f>`__
      -  scl_get_nw_parameters() :
         `scl_ipc.h <group__communication.html#ga690f0ae086500cbc92390cb2c0d0a429>`__
      -  scl_host_buffer_get() :
         `scl_buffer_api.h <scl__buffer__api_8h.html#ad46d8cab5cb70bcdbb7d1168c5a71976>`__
      -  scl_init() :
         `scl_ipc.h <group__communication.html#gaa0e2decd5df1e776b053ddf33e7cd16d>`__
      -  scl_network_process_ethernet_data() :
         `scl_wifi_api.h <group__wifi.html#ga6ffc9c85fcef342276617b83afa610a7>`__
      -  scl_network_send_ethernet_data() :
         `scl_wifi_api.h <group__wifi.html#ga056399731dbe8a81c76186a0ee9626d6>`__
      -  scl_send_data() :
         `scl_ipc.h <group__communication.html#ga4cf63cea8e5e196353b80d769b7182ff>`__
      -  scl_wifi_get_bssid() :
         `scl_wifi_api.h <group__wifi.html#gaa10fb0d690e07935fcb5c2ec35c2029b>`__
      -  scl_wifi_get_mac_address() :
         `scl_wifi_api.h <group__wifi.html#gaeab95b84a48631c4ad535d664c990a43>`__
      -  scl_wifi_get_rssi() :
         `scl_wifi_api.h <group__wifi.html#gaadf11fa39efd4aeb7d4b9efc396f6c24>`__
      -  scl_wifi_is_ready_to_transceive() :
         `scl_wifi_api.h <group__wifi.html#ga7ee9f25797137c03c2f1ad08481b11cf>`__
      -  scl_wifi_on() :
         `scl_wifi_api.h <group__wifi.html#gabb29fb3b65d9a0bffe66afea8d3244da>`__
      -  scl_wifi_register_multicast_address() :
         `scl_wifi_api.h <group__wifi.html#ga22cc8cb41c67b3a3cc1fbcdabb3b6b3f>`__
      -  scl_wifi_scan() :
         `scl_wifi_api.h <group__wifi.html#gaed2249e7300ad43ef519857cd04e4714>`__
      -  scl_wifi_set_up() :
         `scl_wifi_api.h <group__wifi.html#ga5f30d059fce225b6a1bb1d6b63747fa5>`__


