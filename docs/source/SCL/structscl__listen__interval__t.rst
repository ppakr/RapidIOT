======================
scl_listen_interval_t 
======================

.. doxygenstruct:: scl_listen_interval_t
   :project: SCL
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
